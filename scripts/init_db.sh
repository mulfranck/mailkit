#!/usr/bin/env bash

# xecute command in "xtrace" mode or "debug" or "trace" mode.
# Display the command being executed and its result
set -x
#
# -e xecute commands in "exit immediately" mode in case of any non-zero exit status
# -o have the exit status code of a pipeline be the one returning the exit instead
# of the default being the last command along the pipeline
set -eo pipefail

# Verify if the dependencies for this script is already installed

if ! [ -x "$(command -v psql)" ]; then
  echo >&2 "Error: psql is not installed."
  exit 1
fi

if ! [ -x "$(command -v sqlx)" ]; then 
  echo >&2 "Error: sqlx is not installed."
  echo >&2 "Use:"
  echo >&2 "    cargo install --version=0.5.7 sqlx-cli --no-default-features --features postgres"
  echo >&2 "to install it."
  exit 1
fi
 

# Check if a custom user has been set, else default to `postgres`
#echo "Setting default user to: \"postgres\""
# DB_USER=${POSTGRES_USER:=postgres}
 DB_USER=${POSTGRES_USER:=mailkit}


# Check if a custom password has been set, else default to `password`
#echo "Setting default password to: \"password\""
# DB_PASSWORD="${POSTGRES_PASSWORD:=password}"
DB_PASSWORD="${POSTGRES_PASSWORD:=joes5niga}"

# check if a custom database name has been set, else default to `mailkits`
echo "Setting default database: \"mailkits\""
DB_NAME="${POSTGRES_DB:=mailkits}"

# check if a custom post has been set, else default to '5432'
#echo "Setting database post to: 5432"
DB_PORT="${POSTGRES_PORT:=45432}"

# Launch postgres using Docker
#echo "Preparing docker ..."
#echo "Initializing postgres with docker ..."
# Allow to skip Docker if a dockerized Postgres is already running
# if [[ -z "${SKIP_DOCKER}" ]]; then
#   sudo docker run \
#     -e POSTGRES_USER=${DB_USER} \
#     -e POSTGRES_PASSWORD=${DB_PASSWORD} \
#     -e POSTGRES_DB=${DB_NAME} \
#     -p "${DB_PORT}":5432 \
#     -d postgres \
#     postgres
#           # ^ Increased max number of connections for testing purposes
# fi

export PGPASSWORD="${DB_PASSWORD}"

# Try connecting to the database to make sure its ready before 
# kick starting sqlx
until  psql -h "${HOST}" -U "${DB_USER}" -p "${DB_PORT}" -d "postgres" -c '\q'; do
  >&2 echo "Postgres is still unabailable - sleeping"
  sleep 1
done

>&2 echo -e "Postgres is up and running on post ${DB_PORT} ...\nRunning migrations now!"

# Export the DAtabase url as env variable for use with the sqlx database create process
export DATABASE_URL=postgres://${DB_USER}:${DB_PASSWORD}@${HOST}:${DB_PORT}/${DB_NAME}

# sqlx relies on the `DATABASE_URL` env var to work
sqlx database create

# kick start the migration 
sqlx migrate run

>&2 echo "Postgres has been migrated, ready to go!"
#echo "DATABASE_URL:$DATABASE_URL"
