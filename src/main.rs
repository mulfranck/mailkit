use std::net::TcpListener;
use sqlx::PgPool;

use mailkit::config::get_config;
use mailkit::startup::run;

// #[actix_web::main]
#[tokio::main]
async fn main() -> std::io::Result<()> {
    // Panic if we can't read the configuration
    let conf = get_config().expect("Failed to read configuration.");
    let connection = PgPool::connect(&conf.database.conn_string())
        .await
        .expect("Failed to connect to Postgress!");
        
    let addr = format!("127.0.0.1:{}", conf.port);
    let listener = TcpListener::bind(addr).expect("Failed to bind to port 8000");

    // Propagate the io::Error to mailkit::run to handle
    // else, we call the server on await - thus it listen
    // always for requests
    run(listener, connection)?.await
}
