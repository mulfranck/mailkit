// File: config.rs
// The simplest way to connect to the Postgres database is usx PgConnection.
//PgConnection implements the `Connection` trait providing the `connect` method: that requires a connection string and returns, an asynchronously, Result<PostgresConnection, sqlx::Error>.
//Where do we get a connection string?
// We could hard-code one in our application and then use it for our tests as well. Or we could choose to introduce immediately some basic mechanism of configuration management.
// The `config` crate supports multiple file formats and it allow combining different sources hierarchically (e.g. environment variables, configuration files, etc.) to easily customise the behaviour of your application for each environment (e.g. development, staging, production).

#[derive(serde::Deserialize)]
pub struct Settings {
    pub database: DatabaseSettings,
    pub port: u16,
}

#[derive(serde::Deserialize)]
pub struct DatabaseSettings {
    pub username: String,
    pub password: String,
    pub host: String,
    pub port: u16,
    pub db_name: String,
}

impl DatabaseSettings {
    pub fn conn_string(&self) -> String {
        let this = format!(
            "postgres://{}:{}@{}:{}/{}",
            self.username, self.password, self.host, self.port, self.db_name
        );

        this
    }
}

pub fn get_config() -> Result<Settings, config::ConfigError> {
    // Initialize our configuration reader
    let mut settings = config::Config::default();
    // Add the configuration values from a file named `configuration`
    // The file type will be determined from the file extension
    settings
        .merge(config::File::with_name("configuration"))
        .expect("Error: Here");

    // Try to convert the configuration values it read into our Settings type
    settings.try_into()
}
