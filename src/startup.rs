
use std::net::TcpListener;
use actix_web::dev::Server;
use actix_web::{web, App, HttpServer};
use sqlx::PgPool;

use crate::routes::{health_check, subscribe};

pub fn run(
    listener: TcpListener, 
    db_pool: PgPool
    ) -> Result<Server, std::io::Error> {
        // Wrap connection into a smart pointer, ARC
        let db_pool = web::Data::new(db_pool);
        // capture `connection` from the surrounding env
        let server = HttpServer::new(move || {
            App::new()
                .route("/health_check", web::get().to(health_check))
                .route("/subscribe", web::post().to(subscribe))
                // Register the connection's pointer as part of the application state data
                .app_data(db_pool.clone())
    })
    .listen(listener)?
    .run();
    
    Ok(server)
}