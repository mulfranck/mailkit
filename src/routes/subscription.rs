use actix_web::{web, HttpResponse};
use chrono::Utc; // for timestamp
use uuid::Uuid;
use sqlx::PgPool;

#[derive(serde::Deserialize)]
#[allow(dead_code)]
pub struct FormData {
    email: String,
    username: String,
}

pub async fn subscribe(
    form: web::Form<FormData>,
    pool: web::Data<PgPool>, // extract the connection pointer from run()
    ) -> HttpResponse {
        match sqlx::query!(
            r#"
            INSERT INTO subscriptions (id, email, username, subscribed_at)
            VALUES ($1, $2, $3, $4)
            "#,
            Uuid::new_v4(),
            form.email,
            form.username,
            Utc::now()
        )
        // get an immotable ref to the PgConnection wrapped by `web::data`
        .execute(pool.get_ref())
        .await {
            Ok(_) => HttpResponse::Ok().finish(),
            Err(e) => {
                println!("Failed to execute query: {}", e);
                HttpResponse::InternalServerError().finish()
            }
        }
}
