use sqlx::PgPool;
use std::net::TcpListener;

use mailkit::config::get_config;
use mailkit::startup::run;

pub struct TestApp {
    pub adr: String,
    pub db_pool: PgPool,
}

pub async fn spawn_app() -> TestApp {
    // Bind to a random port, using the special :0
    let listener = TcpListener::bind("127.0.0.1:0")
        .expect("Failed to bind random port");
    
    // Get the port number
    let port = listener.local_addr().unwrap().port();
    // Formate the adr string
    let adr = format!(
        "http://127.0.0.1:{}",
        port
    );
    
    // Get configuration
    let config = get_config().expect("Failed to read the configuration file.");
    let conn_pool = PgPool::connect(&config.database.conn_string())
        .await
        .expect("Failed to connecto the db");
        
    // Kick start the server
    let server = run(listener, conn_pool.clone())
        .expect("faild to bind address");
        
    // Send the execution of the server in the future
    let _ = tokio::spawn(server);
    
    TestApp {
        adr,
        db_pool: conn_pool,
    }
}