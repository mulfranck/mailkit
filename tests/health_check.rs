mod app;

use app::spawn_app;
// use mailkit::config::get_config;
// use mailkit::startup::run;

// use sqlx::PgPool;



#[actix_web::test]
async fn health_check_work() {
    // Arrange
    let app = spawn_app().await;

    let client = reqwest::Client::new();
    let url = format!("{}/health_check", &app.adr);

    // Act
    let res = client
        .get(&url)
        .send()
        .await
        .expect("Failed to execute request");

    // Verify or assert!

    assert_eq!(200, res.status().as_u16());
}

// async fn spawn_app() -> TestApp {
//     // To handle the propagate io::Err we expect()
//     let listener = TcpListener::bind("127.0.0.1:0").expect("Failed to bind random port");
//     let port = listener.local_addr().unwrap().port();

//     let adr = format!("http://127.0.0.1:{}", port);
    
//     let config = get_config().expect("Failed to read configuration.");
//     let conn_pool = PgPool::connect(&config.database.conn_string()).await.expect("Failed to connect to Postgres");
        
//     let server = run(listener, conn_pool.clone()).expect("Failed to bind address");

//     // Launch the server as a background task
//     //
//     let _ = tokio::spawn(server);
    
    
//     TestApp {
//         adr,
//         db_pool: conn_pool,
//     }
// }
