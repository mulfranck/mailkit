mod app;

use app::spawn_app;


#[actix_web::test]
async fn subscribe_200_return() {
    let app = spawn_app().await;
    let url = format!("{}/subscribe", &app.adr);

    let client = reqwest::Client::new();

    let body = "username=nigga%20franck&email=niggafranck%40gmail.com";

    let res = client
        .post(&url)
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(body)
        .send()
        .await
        .expect("Fail to execute post request");
    //
    assert_eq!(
        200,
        res.status().as_u16(),
        "The server did not return a 200 from {}",
        url
    );
    
    let saved = sqlx::query!("SELECT email, username FROM subscriptions",)
        .fetch_one(&app.db_pool)
        .await
        .expect("Failed to query data data!");
        
        
    assert_eq!(saved.email, "niggafranck@gmail.com");
    assert_eq!(saved.username, "nigga franck");
}

#[actix_web::test]
async fn subscribe_400_invalid_field() {
    let app = spawn_app().await;
    let url = format!("{}/subscribe", &app.adr);
    let client = reqwest::Client::new();

    let test_cases = vec![
        ("name=justin", "missing the email field"),
        ("email=justin@gmail.com", "missing the name field"),
        ("", "missing both name and email field"),
    ];

    for (invalid_body, err_msg) in test_cases {
        let res = client
            .post(&url)
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(invalid_body)
            .send()
            .await
            .expect("Fail to execute post request");
        // this this
        assert_eq!(
            400,
            res.status().as_u16(),
            "The server did not return a 400 when the payload was {}.",
            err_msg
        );
    }
}
